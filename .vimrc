" Setting Basic Options
set number relativenumber
syntax on
"set pastetoggle=<F2>

" Tabs and indenting
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set smarttab
set bri "preserve indentation in wrapped text

" Switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  set hlsearch
endif

" Put these in an autocmd group, so that we can delete them easily.
augroup vimrcEx
  au!

" For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78
augroup END

"Plugins
"call plug#begin('~/.vim/plugged')
"    Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
"	Plug 'https://github.com/adelarsq/vim-matchit'
"	Plug 'mattn/emmet-vim'
"	Plug 'tpope/vim-commentary'
"	Plug 'tpope/vim-fugitive'
"	Plug 'sainnhe/sonokai'
"	Plug 'vim-airline/vim-airline'
"call plug#end()

" The matchit plugin makes the % command work better, but it is not backwards
" compatible.
" The ! means the package won't be loaded right away but when plugins are
" loaded during initialization.
"--------Templates-------

"nmap ,pan :-1read $HOME/.vim/snippets/pandoc.md<CR>
"nmap ,c :-1read $HOME/.vim/snippets/template.c<CR>

"--------ColorScheme-------

" Solving kitty terminal background bug.
" vim hardcodes background color erase even if the terminfo file does
" not contain bce (not to mention that libvte based terminals
" incorrectly contain bce in their terminfo files). This causes
" incorrect background rendering when using a color theme with a
" background color.
" let &t_ut=''

"Sonokai Theme Configuration
if has('termguicolors')
      set termguicolors
endif

if &term == "alacritty"        
  let &term = "xterm-256color"
endif 

let g:sonokai_style = 'atlantis'
let g:sonokai_better_performance = 1

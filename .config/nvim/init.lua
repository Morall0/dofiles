require("krunker_prp")

vim.cmd('source ~/.vimrc')
vim.cmd('set updatetime=500')
vim.opt.exrc = true
vim.g.editorconfig = true

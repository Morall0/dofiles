vim.g.mapleader = ","
vim.keymap.set("n", "<C-E>", vim.cmd.Ex)
vim.keymap.set("n", ",pan", "<cmd>-1read $HOME/.vim/snippets/pandoc.md<cr>")
vim.keymap.set("n", ",c", "<cmd>-1read $HOME/.vim/snippets/template.c<cr>")
vim.keymap.set("n", ",pp", "<cmd>-1read $HOME/.vim/snippets/template.cpp<cr>")
vim.keymap.set("n", ",java", "<cmd>-1read $HOME/.vim/snippets/java.java<cr>")
vim.keymap.set("n", ",jclass", "<cmd>-1read $HOME/.vim/snippets/class.java<cr>")
vim.keymap.set("n", "sout", "SSystem.out.println(\"\");")
vim.keymap.set({ 'n', 't' }, '<A-d>', '<cmd>Lspsaga term_toggle<CR>', {})

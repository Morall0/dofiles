---
lang: es

# Metadata variables
title: ""
subtitle: ""
author: "Alan Mauricio Morales López"
date: ""

# Variables for LaTeX
documentclass: article
geometry:
- top=25.4mm
- left=25.4mm
- right=25.4mm
- bottom=25.4mm
papersize: letter

numbersections: true
autoEqnLabels: true

# figureTitle: "Figura"
# tableTitle: "Tabla"
# figPrefix: "fig."
# eqnPrefix: "ec."
# tblPrefix: "tbl."
# loftitle: "# Lista de figuras"
# lotTitle: "# Lista de tablas"
...

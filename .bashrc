# .bashrc

# paths
SEMESTRE="/home/krunker_prp/Documents/TercerSemestre/"

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#colors
blk='\[\033[01;30m\]'   # Black
red='\[\033[01;31m\]'   # Red
grn='\[\033[01;32m\]'   # Green
ylw='\[\033[01;33m\]'   # Yellow
blu='\[\033[01;34m\]'   # Blue
pur='\[\033[01;35m\]'   # Purple
cyn='\[\033[01;36m\]'   # Cyan
wht='\[\033[01;37m\]'   # White
clr='\[\033[00m\]'      # Reset

#ls aliases
alias ls='ls --color=auto'
alias la='ls -la'

#cd aliases
alias ..='cd ..'
alias ...='cd ../..'
alias home='cd ~'

#GIT aliases
alias gs='git status'
alias ga='git add'
alias gcm='git commit -m'
alias df='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME' # alias to dotfiles management

#kitty aliases
alias icat="kitty +kitten icat"

#todoo
alias todoo="bash todoo.sh"

eval "$(keychain --eval --quiet --agents ssh id_github)" &>/dev/null
eval "$(pandoc --bash-completion)"

#display the current Git branch in the Bash prompt.

function git_branch() {
    if [ -d .git ] ; then
        printf "%s" "($(git branch 2> /dev/null | awk '/\*/{print $2}')) ";
    fi
}

#setting the prompt.

function bash_prompt() {
    PS1=''${red}'[\u@\h] '${grn}'$(git_branch)'${blu}'\W'${ylw}' \$ '${clr}
}

bash_prompt
#neofetch

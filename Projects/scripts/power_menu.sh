#!/bin/bash

opt=$(printf "Lock\nSuspend\nReboot\nShut Down" | rofi -dmenu -i -p "Power options" -l 4 -theme-str '@import "power_menu.rasi"')

case $opt in
    "Lock")
		loginctl lock-session	
    ;;
    "Suspend")
		loginctl suspend
    ;;
    "Reboot")
		loginctl reboot
    ;;
    "Shut Down")
		loginctl poweroff
    ;;
    *)
        exit 1
    ;;
esac

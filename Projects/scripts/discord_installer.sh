#! /bin/bash

installer=$(ls "$HOME/Downloads/" | grep discord)
if [ -e "$HOME/Downloads/$installer" ]; then
    if [ -e "/usr/local/bin/Discord" ]; then
        sudo rm /usr/local/bin/Discord
    fi
    if [ -d "/usr/share/Discord" ]; then
        sudo rm -r /usr/share/Discord
    fi
    sudo tar -xzf "$HOME/Downloads/$installer" -C /usr/share/
    sudo ln -s /usr/share/Discord/Discord /usr/local/bin/
    echo "Discord has been installed successfully."

    sudo rm "$HOME/Downloads/$installer"
else
    echo "Installer can't be found..."
fi        
